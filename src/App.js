import React from 'react';

import './App.css';
import Admin from "./components/pages/admin";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import MainPage from "./components/pages/MainPage";
import Header from "./components/Header/Header";
import PostPage from "./components/Post/PostPage";
import AuthLogIn from "./components/Auth/AuthLogIn";
import KGnews from "./components/pages/KGnews";
import WorldNews from "./components/pages/worldNews";
import PostKgNews from './components/pages/postKgnews'


function App() {
  return (
      <div>
          <Header nameBar='Kara-Balta News Time'/>
          <BrowserRouter>
              <Switch>
                  <Route path='/admin' component={Admin}/>
                  {/*<Route path='/form' component={AuthLogIn}/>*/}
                  <Route path='/world' component={WorldNews}/>
                  <Route path='/kgnews/:id' component={PostKgNews}/>
                  <Route path='/kgnews/' component={KGnews}/>
                  <Route path='/posts/:id' component={PostPage}/>
                  <Route path='/' component={MainPage}/>
              </Switch>
          </BrowserRouter>
      </div>
  );
}

export default App;
