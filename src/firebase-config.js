import firebase from "firebase/app";
import 'firebase/storage';

const config = {
    apiKey: "AIzaSyDC-HuoO8pwmaasTHiLdZjJFzLKJsRv9Lk",
    authDomain: "newstime-f47b9.firebaseapp.com",
    databaseURL: "https://newstime-f47b9.firebaseio.com",
    projectId: "newstime-f47b9",
    storageBucket: "newstime-f47b9.appspot.com",
    messagingSenderId: "168398335768",
    appId: "1:168398335768:web:0cc6c8440936e81434786d",
    measurementId: "G-2GVLYB5D8Y"
};

firebase.initializeApp(config);

const storage = firebase.storage();

export {
    storage, firebase as default
};