import React from 'react';


import './Header.css';
import Menu from "./Menu";

// var NavStyle = {
//     width: '15450px',
//     backgroundColor: '#58A4B0',
// };


const Header = (props) => {
    return (
        <div className='box'>
            <div className="text-bar">
                <h1>{props.nameBar}</h1>
            </div>
            <div className="Menu">
                <Menu/>
            </div>
        </div>
    );
};

export default Header;