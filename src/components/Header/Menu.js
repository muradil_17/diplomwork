import React, {Component} from 'react';

import './Menu.css'


class Menu extends Component {
    render() {
        return (
                    <div className="nav-content">
                        <a href='/' className='page'>Новости</a>
                        <a href='/kgnews' className='page'>В Кыргызстане</a>
                        <a href='/world' className='page'>В мире</a>
                        <a href='/admin'  className='page'>Администрация</a>
                    </div>
        );
    }
}

export default Menu;