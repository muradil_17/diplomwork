import React, {Component} from 'react';
import Post from "../Post/Post";

import './world.css'

class WorldNews extends Component {
    render() {
        return (
            <div className='world-content'>
                <h1>В мире</h1>
                <Post/>
            </div>
        );
    }
}

export default WorldNews;