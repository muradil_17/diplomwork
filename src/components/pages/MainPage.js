import React, {Component} from 'react';

import './Mainpage.css';
import Post from "../Post/Post";
import FooterNav from "../Header/footerNav";


class MainPage extends Component {
    render() {
        return (
            <div className="App">
                <div className="footer">
                    <FooterNav/>
                </div>
                <div className="posts-content">
                    <Post/>
                </div>
            </div>
        );
    }
}

export default MainPage;