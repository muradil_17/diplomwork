import React, {Component} from 'react';
import axios from './../../axios';
import {storage} from "./../../firebase-config";
import Post from "../Post/Post";

import './admin.css'
import KGnews from "./KGnews";
import ModalWindow from "../Modal/modalWindow";


class Admin extends Component {
    state={
      image: null,
      text: '',
      title: '',
      url: '',
      showModal: false
    };

    valueChanged = (event) =>{
            const image = event.target.files[0];
            this.setState(()=>({image}))
    };

    textValueChanged = (e) =>{
        this.setState({[e.target.name]: e.target.value})
    };

    saveImage = () =>{
        const {image} = this.state;
        const uploadTask = storage.ref(`images/${image.name}`).put(image);
        uploadTask.on('state_changed',
            (snapshot)=>{

            },
            (error)=>{

            },
            ()=>{
                storage.ref('images').child(image.name).getDownloadURL().then(url =>{
                    this.setState({url:url})
                })
            });
    };


    showImage = () =>{
        const postAdmin = {
            url: this.state.url,
            text: this.state.text,
            title: this.state.title,
            postLike: {
                like: 0,
                status: false
            }
        };

        if (postAdmin.title === ''){
            return false
        }

        const choose = prompt('куда опубликовать?\n' +
            '1) В Кыргызстан\n'+ '2) В мире');

        if (choose === 'В Кыргызстан'){
            axios.post('/kgnews.json', postAdmin).finally(()=>{
                this.setState({showModal: true})
            })
        }else {
            axios.post('/posts.json', postAdmin).finally(() => {
                this.setState({showModal: true})
            })
        }
    };

    closedModal= () =>{
      this.setState({showModal: false})
    };

    render() {
        console.log(this.state);
        return (
            <div className='admin'>
                <h1>Создание нового поста</h1>
                <div className="admin-post">
                    <input type="text"
                           placeholder='Название'
                           name='title'
                           className='title'
                           onChange={this.textValueChanged}
                           value={this.state.title}
                           autoComplete= 'off'
                    />
                    <input type="text"
                           placeholder='Текст подзаголовской'
                           name='text'
                           className='text'
                           onChange={this.textValueChanged}
                           value={this.state.text}
                           autoComplete='off'
                    />
                    <input type='file'
                           placeholder='Картинка'
                           className='file-input'
                           id='upload'
                           onChange={this.valueChanged}
                    />
                    <label htmlFor="upload">Картинка</label>
                    <div className="btn-admin">
                        <button onClick={this.saveImage}>Сохранить картинку на сервер</button>
                        <button onClick={this.showImage}>Опубликовать пост</button>
                    </div>
                    <img className='img-admin' id='imgUser' src={this.state.url}/>
                </div>
                <div className="posts-content">
                    <h1>Опубликованые посты</h1>
                    <Post/>
                    <KGnews/>
                    <ModalWindow show={this.state.showModal}>
                        <h1>Пост опубликовался</h1>
                        <div className="button-modal">
                            <button onClick={this.closedModal} className='closedModal'>ок</button>
                        </div>
                    </ModalWindow>
                </div>
            </div>
        );
    }
}

export default Admin;