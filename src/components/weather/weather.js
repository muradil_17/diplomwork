import React, {Component} from 'react';

import './weather.css'

const API_KEY = 'd24fe5329183f180988d8b5beaad5b3a';

class Weather extends Component {

    state={
      temp: '',
      icon: '',
      city: '',
      sunrise: '',
      sunset: '',
    };

   componentDidMount = async () =>{
        const api_url = await
            fetch(`https://api.openweathermap.org/data/2.5/weather?q=Bishkek&appid=${API_KEY}`);
        const data = await  api_url.json();


        this.setState({
            temp: data.main.temp,
            icon: data.weather[0].icon,
            city: data.name,
        });

        const icons = `http://openweathermap.org/img/wn/${this.state.icon}@2x.png`;
        this.setState({icon: icons});

    };

    render() {
        return (
            <div className='weather-box'>
                <h1>Погода на сегодня:</h1>
                <div className='weather'>
                    <img src={this.state.icon} alt="weather" className='icon'/>
                    <p>{this.state.temp}°C</p>
                </div>
            </div>
        );
    }
}

export default Weather;