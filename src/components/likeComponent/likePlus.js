import React, {Component} from 'react';
import axios from './../../axios';

class LikePlus extends Component {
    state={
      likeCount: 0,
      status: false
    };

    clickLikePost = (id) =>{
      if (!this.state.status){
          this.setState((prevState) =>{
              return {
                  likeCount: prevState.likeCount + 1,
                  status: true
              };
          });

          const updateLikestatus = {
              likeCount: this.state.likeCount + 1,
              status: this.state.status = true
          };

          axios.put(`posts/${id}/.json`, updateLikestatus)
      } else {
          this.setState((prefState) =>({
              likeCount: prefState.likeCount - 1,
              status: false
          })
          );

          const updateLikestatus = {
              likeCount: this.state.likeCount - 1,
              status: this.state.status = false
          };

          axios.put(`posts/${id}/.json`, updateLikestatus)
      }
    };

    render() {
        const statusLike = this.state.status ? 'dizlike' : 'like';
        return (
            <div>
                <p>{this.state.likeCount}</p>
                <button onClick={this.clickLikePost}>{statusLike}</button>
            </div>
        );
    }
}

export default LikePlus;