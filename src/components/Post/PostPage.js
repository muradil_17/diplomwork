import React, {Component} from 'react';
import axios from './../../axios';

import './PostPage.css'

class PostPage extends Component {
    state = {
      posts: [],
      text: '',
      textHeader: '',
      url: '',
      like: 0,
      status: false
    };

    componentDidMount() {
        axios.get(`posts/${this.props.match.params.id}/.json`).then(res =>{
            console.log(res);
            const postsAdmin= [];
            postsAdmin.push({...res.data});
            this.setState({posts: postsAdmin})
        });
        axios.get(`posts/${this.props.match.params.id}/postLike.json`).then(res =>{
            const like = res.data.like;
            this.setState({like: like})
        })
    }

    likeFunction = () => {
        const status = this.state.status;
        if (status === false){
            this.setState({
                like: this.state.like + 1,
                status: true
            });
            const likeplus = {
                like: this.state.like + 1,
            };
            const updateLikestatus = {
                status: this.state.status = true
            };
            axios.patch(`posts/${this.props.match.params.id}/postLike/.json`, updateLikestatus);
            axios.patch(`posts/${this.props.match.params.id}/postLike/.json`, likeplus);
        }else {
            this.setState({
                like: this.state.like - 1,
                status: false
            });
            const likeplus = {
                like: this.state.like - 1,
            };
            const updateLikestatus = {
                status: this.state.status = false
            };
            axios.patch(`posts/${this.props.match.params.id}/postLike/.json`, updateLikestatus);
            axios.patch(`posts/${this.props.match.params.id}/postLike/.json`, likeplus);
        }
    };

    render() {
        const statusLike = this.state.status ? 'dizlike' : 'like';
        let postsShow = this.state.posts.map((posts, j)=>{
            return(
                <div className='pages' key={j}>
                    <div className="img-content">
                        <img src={posts.url} alt="postIMG"/>
                    </div>
                    <div className="text-content">
                        <h1>{posts.title}</h1>
                        <p>{posts.text}</p>
                    </div>
                    <div className="like-content">
                        <p>нравиться: {this.state.like}</p>
                        <button onClick={this.likeFunction} className='like'>{statusLike}</button>
                    </div>
                </div>
            )
        });
        return (
            <div>
                {postsShow}
            </div>
        );
    }
}

export default PostPage;