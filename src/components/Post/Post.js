import React, {Component} from 'react';
import axios from './../../axios';
import {Link} from "react-router-dom";
import './post.css';
import ModalWindow from "../Modal/modalWindow";

class Post extends Component {

    state={
      posts: [],
      title: '',
      text: '',
      url: '',
      date: '',
      like: 0,
      PostId: 0,
      showModal: false
    };

    componentDidMount() {
        axios.get('posts.json').then(response => {
            const postsSever = [];
            for (let key in response.data){
                postsSever.push({...response.data[key], id: key});
            }
            this.setState({posts: postsSever, PostId: postsSever.length})
        });

        const date = new Date();
        const postDate = date.getDate()+ "." + (date.getMonth() + 1) + "." + date.getFullYear();
        this.setState({date: postDate});

    }

    removeRecord = (id) => {
        const posts = [...this.state.posts];
        const index = posts.findIndex(post => post.id === id);
        posts.splice(index, 1);
        this.setState({
            posts: posts,
            PostId: this.state.PostId - 1
        });

        axios.delete(`posts/${id}/.json `,
        ).then(() =>{
                this.setState({showModal: true})
            }
        );
    };

    closedModal = () =>{
      this.setState({showModal: false})
    };

    render() {
        const sort = this.state.posts.reverse();
        let posts = sort.map((post, i)=>{
           return(
            <div className="post-main" key={i}>
                <img src={post.url} alt="img" id='imaUser'/>
                <div className="post-content">
                    <div className="post-box">
                        <div className="text-header">
                            <p>Дата публикации: {this.state.date}</p>
                            <Link to={`/posts/${post.id}`}>{post.title}</Link>
                        </div>
                    </div>
                    <div className="post-text">
                        <p>{post.text}</p>
                    </div>
                </div>
                <button className='button red'  onClick={() => this.removeRecord(post.id)}>X</button>
            </div>
           )
        });
        return (
            <div>
                {posts}
                <ModalWindow show={this.state.showModal}>
                    <h1>Пост удалился</h1>
                    <div className="button-modal">
                        <button onClick={this.closedModal} className='closedModal'>ок</button>
                    </div>
                </ModalWindow>
            </div>
        );
    }
}

export default Post;