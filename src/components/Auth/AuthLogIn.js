import React, {Component} from 'react';
import axios from './../../axios'

class AuthLogIn extends Component {
    state = {
        login: '',
        password: '',
        status: true,
        admin: []
    };

    changeHand = (ev) =>{
        this.setState({
            [ev.target.name]: ev.target.value
        });
    };

    checkLoginStatus = (e) =>{
        e.preventDefault();
        axios.get('admin.json').then(res=>{
            if (res.data.login === this.state.login
                && res.data.password === parseInt(this.state.password)
                && res.data.status === this.state.status ){
                this.props.history.push({
                    pathname: '/admin'
                })
            }else{
                alert('wrong login or password')
            }
        })
    };




    render() {
        console.log(this.props);
        return (
            <form>
                <input type="text"
                       name='login'
                       placeholder='Login'
                       autoComplete='off'
                       onChange={this.changeHand}
                />
                <input type="text"
                       name='password'
                       placeholder='Password'
                       autoComplete='off'
                       onChange={this.changeHand}
                />
                <button onClick={this.checkLoginStatus}>Log in</button>
            </form>
        );
    }
}

export default AuthLogIn;