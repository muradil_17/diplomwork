import axios from "axios";

const baseWeather = axios.create({
    baseURL: "https://newstime-f47b9.firebaseio.com/",
});

export default baseWeather;